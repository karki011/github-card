import React from 'react';
import { Card, Image } from 'semantic-ui-react';

const UserCard = (props) => (
	<div className="mainUserCard">
		<Card className="userCard">
			<Image src={props.image} wrapped ui={false} />
			<Card.Content>
				<Card.Header>{props.header}</Card.Header>
				<Card.Meta>
					<span className="date">{props.location}</span>
				</Card.Meta>

				<Card.Meta>
					<span className="date">{props.created_at}</span>
				</Card.Meta>
			</Card.Content>
			<Card.Content>
				<Card.Meta>Followers: {props.followers}</Card.Meta>
				<Card.Meta>Following: {props.following}</Card.Meta>
				<Card.Meta>
					<span className="Description">{props.repos_url}</span>
				</Card.Meta>
			</Card.Content>
		</Card>
	</div>
);

export default UserCard;
