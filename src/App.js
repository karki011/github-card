import React, { Component } from 'react';
import './App.css';
import ButtonAnimated from './ButtonAnimated';
import UserCard from './UserCard';

const GITHUB_URL = (userName) => `https://api.github.com/users/${userName}`;

class App extends Component {
	state = {
		user: {},
		toggleActive: false
	};
	handleClick = () => {
		console.log('clicked');

		if (this.state.toggleActive) {
			this.setState({ toggleActive: false });
		} else {
			fetch(GITHUB_URL('mrkhutter')).then((res) => res.json()).then((data) => {
				this.setState({ user: data, toggleActive: true });
			});
		}
	};
	render() {
		const userData = this.state.user;
		return (
			<div>
				<ButtonAnimated onClick={this.handleClick} />
				{this.state.toggleActive && (
					<UserCard
						image={userData.avatar_url}
						header={userData.name}
						location={userData.location}
						created_at={userData.created_at}
						followers={userData.followers}
						following={userData.following}
						repos_url={userData.repos_url}
					/>
				)}
			</div>
		);
	}
}

export default App;
