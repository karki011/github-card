import React from 'react';
import { Button } from 'semantic-ui-react';

const ButtonAnimated = (props) => (
	<div className="buttonToggle">
		<Button animated="fade">
			<Button.Content visible>Toogle Me</Button.Content>
			<Button.Content hidden onClick={props.onClick}>
				Click Me!!!
			</Button.Content>
		</Button>
	</div>
);
export default ButtonAnimated;
